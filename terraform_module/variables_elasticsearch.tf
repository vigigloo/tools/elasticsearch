variable "elasticsearch_replicas" {
  type    = number
  default = 3
}
variable "elasticsearch_min_master_nodes" {
  type    = number
  default = 2
}
variable "elasticsearch_persistence_size" {
  type        = string
  description = "Size for the persistent disks, for example `30Gi`"
}
