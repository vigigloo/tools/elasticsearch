resource "helm_release" "elasticsearch" {
  chart           = "elasticsearch"
  repository      = "https://helm.elastic.co"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/elasticsearch.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.image_repository == null ? [] : [var.image_repository]
    content {
      name  = "image"
      value = var.image_repository
    }
  }
  dynamic "set" {
    for_each = var.image_tag == null ? [] : [var.image_tag]
    content {
      name  = "imageTag"
      value = var.image_tag
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "resources.limits.cpu"
      value = var.limits_cpu
    }
  }
  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "resources.limits.memory"
      value = var.limits_memory
    }
  }
  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "resources.requests.cpu"
      value = var.requests_cpu
    }
  }
  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "resources.requests.memory"
      value = var.requests_memory
    }
  }

  dynamic "set" {
    for_each = var.elasticsearch_replicas == null ? [] : [var.elasticsearch_replicas]
    content {
      name  = "replicas"
      value = var.elasticsearch_replicas
    }
  }
  dynamic "set" {
    for_each = var.elasticsearch_min_master_nodes == null ? [] : [var.elasticsearch_min_master_nodes]
    content {
      name  = "minimumMasterNodes"
      value = var.elasticsearch_min_master_nodes
    }
  }
  dynamic "set" {
    for_each = var.elasticsearch_persistence_size == null ? [] : [var.elasticsearch_persistence_size]
    content {
      name  = "volumeClaimTemplate.resources.requests.storage"
      value = var.elasticsearch_persistence_size
    }
  }
}
