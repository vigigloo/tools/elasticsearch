ARG BASE_IMAGE=docker.elastic.co/elasticsearch/elasticsearch:8.9.0
FROM $BASE_IMAGE

ARG PLUGINS
RUN if [ -n "$PLUGINS" ]; then /usr/share/elasticsearch/bin/elasticsearch-plugin install --batch $PLUGINS; fi
